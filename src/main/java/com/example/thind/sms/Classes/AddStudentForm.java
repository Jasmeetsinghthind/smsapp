package com.example.thind.sms.Classes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.thind.sms.R;

public class AddStudentForm extends AppCompatActivity implements View.OnClickListener{
    EditText enterName = null;
    EditText enterEmail = null;
    EditText enterPhone = null;
    EditText enterRollNo = null;
    Student student;
    boolean isEditable;
    boolean isedit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student_form);
        enterName = (EditText)findViewById(R.id.enterName);
        enterEmail = (EditText)findViewById(R.id.enterEmail);
        enterPhone = (EditText)findViewById(R.id.enterPhone);
        enterRollNo = (EditText)findViewById(R.id.enterRollNo);
        final Button cancel=(Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
       final Button save=(Button) findViewById(R.id.save);
        save.setOnClickListener(this);
    if(getIntent().hasExtra("vi")){
        student =(Student) getIntent().getSerializableExtra("vi");
        isEditable =getIntent().getBooleanExtra("edit",true);
        isedit =getIntent().getBooleanExtra("edit",false);
    }
    if(student != null)
    {
        enterName.setText(student.getName());
        enterEmail.setText(student.getEmail());
        enterPhone.setText(String.valueOf(student.getPhone()));
        enterRollNo.setText(String.valueOf(student.getRollno()));
        enterName.setEnabled(isEditable);
        enterEmail.setEnabled(isEditable);
        enterPhone.setEnabled(isEditable);
        enterRollNo.setEnabled(false);
    if(!isEditable){
        save.setVisibility(View.INVISIBLE);
    }
    else{
     save.setVisibility(View.VISIBLE);
    }
    }

    }

    @Override
    public void onClick(View v) {
        String stuName = enterName.getText().toString();
        String stuRoll = enterRollNo.getText().toString();
        String stuPhone = enterPhone.getText().toString();
        String stuEmail = enterEmail.getText().toString();
switch (v.getId())
{

case R.id.save:
      if((!stuName.isEmpty()) && (!stuRoll.isEmpty()) && (!stuPhone.isEmpty()) && (!stuEmail.isEmpty()) )
        {
            Intent intent = new Intent();
            if(isEditable)
            {
                student.setName(stuName);
                student.setRollno(Long.parseLong(stuRoll));
                student.setPhone(Long.parseLong(stuPhone));
                student.setEmail(stuEmail);
                intent.putExtra("vi",student);
            }

            intent.putExtra("name",stuName);
            intent.putExtra("rn",stuRoll);
            intent.putExtra("phone",stuPhone);
            intent.putExtra("email",stuEmail);
            setResult(RESULT_OK, intent);
            finish();
        }

    break;
case R.id.cancel:
    setResult(RESULT_CANCELED);
    finish();
        }
    }
}