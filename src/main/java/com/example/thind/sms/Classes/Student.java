package com.example.thind.sms.Classes;

import java.io.Serializable;

/**
 * Created by Thind on 07-09-2015.
 */
public class Student implements Serializable{
public String name,email;
   public long rollno,phone;

    public long getRollno() {return rollno;}

    public void setRollno(long rollno) {this.rollno = rollno;}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;

    }

    public void setName(String name) {
        this.name = name;
    }
}
