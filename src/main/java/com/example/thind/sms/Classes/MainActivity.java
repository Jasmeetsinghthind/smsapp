package com.example.thind.sms.Classes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.thind.sms.Adapter.Adapter;
import com.example.thind.sms.Comparator.ById;
import com.example.thind.sms.Comparator.ByName;
import com.example.thind.sms.R;
import com.example.thind.sms.util.DialogListener;

import java.util.Collections;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity implements DialogListener {
    Adapter adapter;
    Intent intentObj;
    LinkedList<Student> studentData;
    int pc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button list = (Button) findViewById(R.id.list);
        final Button grid =(Button) findViewById(R.id.grid);
        final Button imageButton = (Button) findViewById(R.id.addButton);
        final ListView listView = (ListView) findViewById(R.id.viewList);
        final GridView gridView = (GridView) findViewById(R.id.viewgrid);
        studentData = new LinkedList<>();
        adapter = new Adapter(studentData, this, R.layout.activity_after_save);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pc=position;
                view();
            }
        });
        grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setVisibility(v.INVISIBLE);
                gridView.setVisibility(v.VISIBLE);
                gridView.setAdapter(adapter);
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                       pc=position;
                        view();
                    }
                });
            }


        });
list.setOnClickListener(new View.OnClickListener(){

                            @Override
                            public void onClick(View v) {
                            gridView.setVisibility(v.INVISIBLE);
                            listView.setVisibility(v.VISIBLE);
                            listView.setAdapter(adapter);
                            }
                        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterName();
            }
        });
    final Spinner spinner=(Spinner)findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
             String sortBy =spinner.getSelectedItem().toString();
                if(sortBy.equals("Name")){
                    Collections.sort(studentData,new ByName());
                    adapter.notifyDataSetChanged();
                } if(sortBy.equals("Roll_No")){
                    Collections.sort(studentData,new ById());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void enterName() {
        intentObj = new Intent(this, AddStudentForm.class);
        startActivityForResult(intentObj, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Student StdData = new Student();
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1:
                    StdData.name = data.getStringExtra("name");
                    StdData.email = data.getStringExtra("email");
                    StdData.rollno = Long.parseLong(data.getStringExtra("rn"));
                    StdData.phone = Long.parseLong(data.getStringExtra("phone"));
                    studentData.add(StdData);
                    adapter.notifyDataSetChanged();
                    break;
                case 10:
                    studentData.set(pc, (Student) data.getSerializableExtra("vi"));
                    break;
               case 20:
                 studentData.set(pc, (Student) data.getSerializableExtra("vi"));
               adapter.notifyDataSetChanged();
               break;
               }
        } else {
            Toast.makeText(this, "Canceled", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void OnClick(int position) {
        switch (position){
            case 1:
                Intent in = new Intent(this,AddStudentForm.class);
                in.putExtra("vi", studentData.get(pc));
                in.putExtra("edit", false);
                startActivityForResult(in,10);
        break;
            case 2:
                Intent in1 = new Intent(this,AddStudentForm.class);
               in1.putExtra("vi", studentData.get(pc));
                in1.putExtra("edit",true);
               startActivityForResult(in1,20);
        //        adapter.notifyDataSetChanged();
                break;
            case 3:
                studentData.remove(pc);
                adapter.notifyDataSetChanged();
                break;
                        }
    }
public void view()
{  // pc = position;
    DialogBox dialog = new DialogBox();
    dialog.setDialogListener(MainActivity.this);
    dialog.show(getFragmentManager(), "Dialog");
}
}
