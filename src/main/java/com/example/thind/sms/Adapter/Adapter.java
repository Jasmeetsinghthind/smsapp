package com.example.thind.sms.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.thind.sms.Classes.Student;
import com.example.thind.sms.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thind on 07-09-2015.
 */
public class Adapter extends BaseAdapter{
    List<Student> data;
    Context context;
    int rowLayout;

    public Adapter(List<Student> data, Context context, int rowLayout) {
        this.data = data;
        this.context = context;
        this.rowLayout = rowLayout;

    }

    @Override
    public int getCount() {
        return data == null?0:data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate( rowLayout, null);
        TextView textName = (TextView) convertView.findViewById(R.id.t1);
        TextView textRoll = (TextView) convertView.findViewById(R.id.t2);
        TextView textPhone = (TextView) convertView.findViewById(R.id.t3);
        TextView textEmail = (TextView) convertView.findViewById(R.id.t4);
        textName.setText(data.get(position).getName());
        textRoll.setText(String.valueOf(data.get(position).getRollno()));
        textPhone.setText(String.valueOf(data.get(position).getPhone()));
        textEmail.setText(data.get(position).getEmail());
        return convertView;
    }
}
